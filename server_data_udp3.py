import asyncio
import json

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE

offer_recibido=""
answer_enviado=""
bye_recibido=""
cliente=""
async def consume_signaling(pc, cliente):
    while True:
        await wait_offer_recibido()
        offer = json.loads(offer_recibido)
        sdp = offer["sdp"]
        obj = RTCSessionDescription(sdp=sdp, type="offer")
        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                    # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                global answer_enviado
                answer_enviado = json.dumps(pc.localDescription.__dict__)
                print("Send: ", answer_enviado)
                cliente.transport.sendto(answer_enviado.encode())
                await wait_bye_recibido()

        if bye_recibido is BYE:
            bye = json.dumps({"type": "bye"})
            print("Send: ", bye)
            cliente.transport.sendto(bye.encode())
            print("En vez cerrar el servidor podras seguir utilizandolo conforme conectes clientes de manera sucesiva")
            reset_variables_globales()
            pc= RTCPeerConnection()
            await run_answer(pc)





time_start = None


async def run_answer(pc):
    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    global cliente
    if cliente == "":
        loop = asyncio.get_running_loop()
        message = "REGISTER SERVER"
        on_con_lost = loop.create_future()
        cliente=EchoClientProtocol(message, on_con_lost)
        await loop.create_datagram_endpoint(lambda: cliente,remote_addr=('127.0.0.1', 9999))
    await consume_signaling(pc, cliente)


async def wait_offer_recibido():
    while offer_recibido=="":
        await asyncio.sleep(1)

async def wait_bye_recibido():
    while bye_recibido=="":
        await asyncio.sleep(1)
def reset_variables_globales():
    global offer_recibido
    global answer_enviado
    global bye_recibido
    offer_recibido = ""
    answer_enviado = ""
    bye_recibido = ""
class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send: REGISTER SERVER')
        self.transport.sendto(self.message.encode())


    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        if data.decode().split('"')[len(data.decode().split('"')) - 2] == "offer":
            # Accept the offer
            global offer_recibido
            offer_recibido = data.decode()
        if data.decode().split('"')[len(data.decode().split('"')) - 2] == "bye":
            global bye_recibido
            bye_recibido = BYE



    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_answer(pc)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())

