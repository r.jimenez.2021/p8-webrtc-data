import asyncio
import time
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

answer_recibido=""
ok_recibido=""
async def consume_signaling(pc):
    while True:
        await wait_answer_recibido(pc)
        answer = json.loads(answer_recibido)
        sdp = answer["sdp"]
        obj = RTCSessionDescription(sdp=sdp, type="answer")
        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)
        elif obj is BYE:
            print("Exiting")

time_start = None

def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc):
    channel = pc.createDataChannel("chat")

    async def send_pings():
        print(f"channel({channel.label}) > created by local party")
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    message="REGISTER CLIENT"
    cliente = EchoClientProtocol(message, on_con_lost)
    await loop.create_datagram_endpoint(lambda: cliente,remote_addr=('127.0.0.1', 9999))
    await wait_ok()
    message_offer = json.dumps(pc.localDescription.__dict__)
    print("Send: ", message_offer)
    cliente.transport.sendto(message_offer.encode())
    await consume_signaling(pc)
async def wait_answer_recibido(pc):
    while answer_recibido=="" or not pc.remoteDescription is None:
        await asyncio.sleep(1)
async def wait_ok():
    while ok_recibido== "":
        await asyncio.sleep(1)
class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        if data.decode().split('"')[len(data.decode().split('"')) - 2] == "answer":
            # Accept the offer
            global answer_recibido
            answer_recibido = data.decode()
        if data.decode() == "OK":
            global ok_recibido
            ok_recibido = data.decode()
    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_offer(pc)
    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
