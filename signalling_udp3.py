import asyncio

clientlist=[]
serverlist=[]
mensaje_no_enviado=[]
class EchoServerProtocol:

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if message== "REGISTER CLIENT":
            clientlist.append({"Nombre": len(clientlist)+1,"Direccion": addr})
            print('Send %r to %s' % ("OK", addr))
            self.transport.sendto("OK".encode(), addr)
        if message== "REGISTER SERVER":
            serverlist.append({"Nombre": len(serverlist)+1,"Direccion": addr})
            print('Send %r to %s' % ("OK", addr))
            self.transport.sendto("OK".encode(), addr)
            if len(mensaje_no_enviado) != 0:
                print('Send %r to %s' % (mensaje_no_enviado[0], serverlist[len(serverlist)-1]["Direccion"]))
                self.transport.sendto(mensaje_no_enviado[0].encode(),serverlist[len(serverlist)-1]["Direccion"])

        if message.split('"')[len(message.split('"'))-2]== "offer":
            try:
                self.transport.sendto(message.encode(),serverlist[len(serverlist)-1]["Direccion"])
                print('Send %r to %s' % (message, serverlist[len(serverlist)-1]["Direccion"]))
            except IndexError:
                print('Send: %r to %s' % ("No hay servidores disponibles", clientlist[len(clientlist)-1]["Direccion"]))
                self.transport.sendto("No hay servidores disponibles, se enviara el mensaje cuando se abra un servidor".encode(), clientlist[len(clientlist)-1]["Direccion"])
                mensaje_no_enviado.append(message)
        if message.split('"')[len(message.split('"'))-2]== "answer":
            print('Send %r to %s' % (message, clientlist[len(clientlist)-1]["Direccion"]))
            self.transport.sendto(message.encode(),clientlist[len(clientlist)-1]["Direccion"])

        if message.split('"')[len(message.split('"'))-2] =="bye":
            if addr == clientlist[len(clientlist)-1]["Direccion"]:
                print("Send %r to %s" % (message, serverlist[len(serverlist)-1]["Direccion"]))
                self.transport.sendto(message.encode(), serverlist[len(serverlist)-1]["Direccion"])
            if addr == serverlist[len(serverlist)-1]["Direccion"]:
                print("Send %r to %s" % (message, clientlist[len(clientlist)-1]["Direccion"]))
                self.transport.sendto(message.encode(), clientlist[len(clientlist)-1]["Direccion"])


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())